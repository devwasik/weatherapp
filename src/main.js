import React from "react"
import Home from './Home'
import { Redirect, NavLink, Switch, Route } from "react-router-dom"

const Main = () => (
    <div>
    <Switch>
        <Route exact path="/" component={Home}/>
        <Redirect from="/" to="Home" />
    </Switch>
    </div>
)

export default Main;