const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
require('dotenv').config();
const path = require("path");
const moment = require("moment")
const isDev = require("electron-is-dev");
const exec = require('child_process').exec

//TODO: Remove from global scope!

let appState = {
  inGame: false,
  sessionStartTime: 0,
  sessionLength: 0,
  currentSessionNotifySent: false
}
// let inGame = false;
// let currentSessionNotifySent = false;
// let sessionStartTime = 0;
// let sessionLength = 0;

/*TODO: 
    Refactor this so dev / prod versions read from different location
    This will probably be something to make an API call to the website for
*/
const data = require(path.join(__dirname, "../src/gamelist.json"));

let mainWindow;

require("update-electron-app")({
  repo: "kitze/react-electron-example",
  updateInterval: "1 hour"
});

function createWindow() {
  mainWindow = new BrowserWindow({ width: 900, height: 680 });
  mainWindow.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "../build/index.html")}`
  );
  mainWindow.on("closed", () => (mainWindow = null));
}

app.on("ready", () => {

  createWindow();
  setInterval(() => {
    main()
  }, 60000)
  main();
})

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});

const isRunning = ((win, mac, linux) => {
  return new Promise((resolve, reject) => {
    const plat = process.platform
    const cmd = plat == 'win32' ? 'tasklist' : (plat == 'darwin' ? 'ps -ax | grep ' + mac : (plat == 'linux' ? 'ps -A' : ''))
    const proc = plat == 'win32' ? win : (plat == 'darwin' ? mac : (plat == 'linux' ? linux : ''))
    if (cmd === '' || proc === '') {
      return resolve(false)
    }
    exec(cmd, function (err, stdout, stderr) {
      let res = stdout.toLowerCase().indexOf(proc.toLowerCase())
      res > -1 ? resolve(true) : resolve(false)
    })

  })
})

const sessionStarted = (game) => {
  console.log('im logging ' + game)
  const eNotify = require('electron-notify');
  if (!appState.inGame) {
    appState.sessionStartTime = moment().unix()
  }

  appState.inGame = true;
  console.log(moment.unix(appState.sessionStartTime).fromNow())
  if (!appState.currentSessionNotifySent) {
    eNotify.setConfig({
      displayTime: 5000
    });
    eNotify.notify({ title: 'New Session Started', text: 'Now playing ' + game + " Session started: " + appState.sessionStartTime });
    appState.currentSessionNotifySent = true;
  }
}

const sessionEnded = () => {
  appState.inGame = false;
  appState.currentSessionNotifySent = false;
  appState.sessionStartTime = 0;
}

const main = async () => {
  let gameFound = false;
  for (let i = 0; i < data.length; i++) {
    let res = await isRunning(data[i].win, data[i].mac, data[i].linux)
    if (res) {
      sessionStarted(data[i].name)
      gameFound = true;
      currentGame = data[i].name;

      break;
    }
  }
  if (!gameFound) {
    sessionEnded(currentGame);
  }
}


